<?php include("theme/theme.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="refresh" content="120">
	<title></title>
</head>
<body id='cocoaBG' style="background-color: #2c3e50;">
<br>
<br>
<center>
	<h3  class="ultralight" id="servertimex" style="color: black !important; margin-bottom: 0px; padding-bottom: 0px; font-size: 60px;"></h3>
	<h4 style="color: white;" class="ultrabold"><i class="far fa-clock"></i> CDTRS BREAK</h4>
	<br>
</center>

<script type="text/javascript">
	setInterval(function(){
		$( "#servertimex" ).load( "func/servertime.php" );
	},100)
</script>
<?php

$ipAddress=$_SERVER['REMOTE_ADDR'];




function GetMAC(){


$ipAddress=$_SERVER['REMOTE_ADDR'];
$macAddr=false;

#run the external command, break output into lines
$arp=`arp -a $ipAddress`;
$lines=explode("\n", $arp);

#look for the output line describing our IP address
foreach($lines as $line)
{
   $cols=preg_split('/\s+/', trim($line));
   if ($cols[0]==$ipAddress)
   {
       $macAddr=$cols[1];
   }
}
if($macAddr == ""){
ob_start(); // Turn on output buffering
system('ipconfig /all'); //Execute external program to display output
$mycom=ob_get_contents(); // Capture the output into a variable
ob_clean(); // Clean (erase) the output buffer
$findme = "Physical";
$pmac = strpos($mycom, $findme); // Find the position of Physical text
$mac=substr($mycom,($pmac+36),17); // Get Physical Address
echo $mac;
}else{
echo $macAddr;
}
}
$macAddr=false;

date_default_timezone_set('Asia/Manila'); 
	$req_date =  date("Y-m-d h:i:s a",strtotime(date("Y-m-d") . " 12:00 pm"));
	$req_date_in =  date("Y-m-d h:i:s a",strtotime(date("Y-m-d") . " 1:00 pm"));

	// echo $req_date_in;
	if(strtotime(date("Y-m-d h:i:s a")) > strtotime($req_date) && strtotime(date("Y-m-d h:i:s a")) < strtotime($req_date_in))
	{
		?>

		<div class="container">
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<div id="logform" style="display: none;">
			
		<div class="card" >
				<div class="card-body">
					<center>
						<img src="theme/sdo.png" style="width: 100px;">
					</center>
					<br>
					<form action="func/func.php" method="POST">
						<div class="form-group">
						<input type="text" required="" autocomplete="off" id="employee_id_number" class="form-control" placeholder="Type your Employee I.D here..." name="employee_id_number">
					</div>
						<input type="hidden" value="" id="logtypeval" name="tag">
							<button onclick="$('#logtypeval').val('TimeOut');" type="submit" style="width: 49%;" class="btn btn-danger"><i class="fas fa-sign-out-alt"></i> OUT</button>
							<button onclick="$('#logtypeval').val('TimeIn');" type="submit" style="width: 49%;" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i> IN</button>
					</form>
				</div>
		</div>
		<center style="color: white"><i class="far fa-question-circle"></i> Online Log only appears between 12pm to 1pm.<br>
			<span style="color: rgba(255,255,255,0.5)"><?php GetMAC(); ?></span></center>
		</div>
		<div id="alertform" style="display: none;">
			<center>
				<br>
				<br>
						<img src="theme/sdo.png" style="width: 100px;">
						<p style="color: white;">This computer is not authorized to use the CDTRS Online Logs.
							<br>
							<br>
							<br>
							<small>Deped Marikina - ICTU<br>
								<span style="color: rgba(255,255,255,0.5)"><?php GetMAC(); ?></span></small>
						</p>
					</center>
			
		</div>
			</div>
			<div class="col-sm-4"></div>
		</div>
		</div>
	<script type="text/javascript">
			$.ajax({
				type: "POST",
				url : "func/func.php",
				data : {tag : "checkifvalidmac"},
				success : function(data){
					// alert(data);
					if(data == "0"){
						$("#logform").css("display","none");
						$("#alertform").css("display","block");
					}else if(data == "1"){
						
						$("#logform").css("display","block");
						$("#alertform").css("display","none");
					}
				}
			})
		</script>
		<?php
	}else{
	    ?>

	   <center>
		<br>
	   	 <h1 style="font-size: 20vh; color: white;">Work.</h1>
	   	 <div style="display: none;" id="setup_btn">
	   	 	<button class="btn btn-primary btn-lg"  data-toggle="modal" data-target="#modal_setuppc"><i class="fas fa-arrow-circle-right"></i> SETUP THIS PC</button><br><br>
	   	 </div>
	   	 


	   	 	<script type="text/javascript">
			$.ajax({
				type: "POST",
				url : "func/func.php",
				data : {tag : "checkifvalidmac"},
				success : function(data){
					if(data == "0"){
						$("#setup_btn").css("display","block");
					}else if(data == "1"){
						$("#setup_btn").css("display","none");
					}
				}
			})
		</script>



	   	 <p style="color: white;">CDTRS BREAK opens in 12pm - 1pm.<br>
	   	 	<small><span style="opacity: 0.05; color: rgba(255,255,255,0.5)"><?php GetMAC(); ?></span></small></p>
	   </center>
	  
	    <?php
	}
?>

<a href="" style="position: fixed; bottom: 0; left: 0; margin: 10px;" data-toggle='modal' data-target="#trobleshootingmodal"><i class="fas fa-universal-access"></i> Division ICTU</a>

<form action="func/func.php" method="POST">
	<div class="modal" tabindex="-1" role="dialog" id="modal_setuppc">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fas fa-bolt"></i> Quick Setup</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="tag" value="quick_setup">
      	<input type="hidden" value="<?php GetMAC(); ?>" name="get_macadd">
      	<div class="form-group">
  		<label>Employee Name</label>
  		<select required="" class="form-control" id="allempnames" name="get_empnum">
  			
  		</select>

  		<script type="text/javascript">
  			$.ajax({
  				type: "POST",
  				url : "func/func/php",
  				data: {tag:"getEmployeeNames"},
  				success: function(data){
  					// alert(data);
  					$("#allempnames").html(data);
  				}
  			})
  		</script>
      	</div>
       <div class="form-group">
       	<label>Username</label>
       	<input required="" autocomplete="off" placeholder="Type here..." type="text" class="form-control" name="get_username">
       </div>
        <div class="form-group">
       	<label>Password</label>
       	<input required="" autocomplete="off" placeholder="Type here..." type="password" class="form-control" name="get_password">
       </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Setup this PC</button>
      </div>
    </div>
  </div>
</div>
</form>


<form action="func/func.php" method="POST">
	<div class="modal" tabindex="-1" role="dialog" id="trobleshootingmodal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Employee Number Verification</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <div class="form-group">
	        	<label>Type an Employee Number</label>
	        	<input required="" type="text" placeholder="Type here..." class="form-control" name="employee_id_number">
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" name="tag" value="verify" class="btn btn-primary"><i class="fas fa-robot"></i> Verify</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
	      </div>
	    </div>
	  </div>
	</div>
</form>
</body>
</html>